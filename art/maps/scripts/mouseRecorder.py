from pynput.mouse import Listener
import os.path
import datetime
mouseRecTxt = os.path.expanduser("~/maps/scripts/data/theMouseRecord.txt")

def is_clicked(x, y, button, pressed):
	if pressed:
		d=datetime.datetime.now()
		dd=d.strftime("%d %m %Y %H %M %S")
		finalString=format(dd)+" "+format(x)+" "+format(y)+"\n"
		theFile=open(mouseRecTxt,"a", encoding="utf-8")
		theFile.write(finalString)
		theFile.close()
		print(dd,x,y)
		
		
with Listener(on_click=is_clicked) as listener:
	listener.join()
