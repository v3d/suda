#/bin/bash

# Populate variables from /etc/os-release to check for distro
source /etc/os-release

# set distro names for Debian and Arch based distros
DISTD=("trisquel" "gnuinos" "debian" "ubuntu" "devuan" "raspbian")
DISTA=("parabola" "arch" "manjaro" "endeavouros")

# set generated password for when we need it
secret=$(tr -dc A-Za-z0-9 </dev/urandom | head -c 13 ; echo '')

# get local IP for LAN setup (IP is the FSF)
ip=$(ip -o route get to 209.51.188.174 | sed -n 's/.*src \([0-9.]\+\).*/\1/p')

# check if server flags are set and execture server setup if they are. 

unset -v domain
unset -v password

while getopts 'd:p:' flag; do
  case "${flag}" in
    d) domain="${OPTARG}" ;;
    p) password="${OPTARG}" ;;
    *)
       exit 1
       ;;
  esac
done
if [ -z ${password+x} ] || [ -z ${domain+x} ]; then
if [[ " ${DISTD[*]} " =~ " ${ID} " ]]; then

    # install for Debian based systems
    echo "SUDA installer 🄯 v3d 2022"
    echo "Documentation: https://wiki.formatc.hr/shelves/suda." 
    read -p "This system is Debian - based. Press Enter to continue installing SUDA."
    echo "Installing software..."
    sudo apt update
    sudo apt install ctwm ffmpeg git icecast2 lxdm lxterminal pd psmisc python3 python3-evdev python3-tk python3-xlib unifont vim wget x11vnc xdotool xterm
    if [[ " $ID " = " trisquel " ]]; then 
    read -p "Trisquel detected, we need to download python3-pynput_1.7.5-1_all.deb from the FSF, press Enter to continue."
    wget -O $HOME/python3-pynput_1.7.5-1_all.deb http://mirror.fsf.org/trisquel/pool/main/p/pynput/python3-pynput_1.7.5-1_all.deb
    sudo dpkg -i $HOME/python3-pynput_1.7.5-1_all.deb
    sudo rm $HOME/python3-pynput_1.7.5-1_all.deb
    else
    sudo apt install python3-pip
    sudo pip install pynput
    fi
    echo "Fetching configs and scripts..."
    git clone https://gitlab.com/hacklab01/suda.git $HOME/suda-git
    echo "Copying ctwm.desktop to /usr/share/xsessions/ so lxdm knows about ctwm..."
    sudo cp $HOME/suda-git/config/ctwm.desktop /usr/share/xsessions/
    echo "Copying .ctwmrc config file to home dir"
    cp $HOME/suda-git/config/.ctwmrc $HOME/
    echo "Copying the lxdm configuration file default.conf to /etc/lxdm overwriting any previous config..."
    sudo cp $HOME/suda-git/config/default.conf /etc/lxdm/default.conf
    echo "Copying scripts to /usr/local/bin and make them executable..."
    sudo cp $HOME/suda-git/scripts/*.sh /usr/local/bin/
    sudo chmod +x /usr/local/bin/*.sh
    echo "Copying PostLogin script for autostart..."
    sudo cp $HOME/suda-git/config/PostLogin /etc/lxdm/
    echo "Fetching noVNC..."
    wget -O $HOME/suda-git/noVNCv1.3.0.zip  https://github.com/novnc/noVNC/archive/refs/tags/v1.3.0.zip
    unzip -q $HOME/suda-git/noVNCv1.3.0.zip -d $HOME/suda-git/
    echo "Setting up SUDA web interface..."
    cp -r $HOME/suda-git/art/web/* $HOME/suda-git/noVNC-1.3.0/
    echo "Copying systemd service files"
    sudo cp $HOME/suda-git/config/*.service /etc/systemd/system/
    sudo systemctl enable novnc.service
    sudo systemctl enable x11vnc.service
    sudo systemctl start novnc.service
    sudo systemctl start x11vnc.service
    # Icecast
    echo "Configuring icecast..."
    sudo cp $HOME/suda-git/config/icecast-local.xml /etc/icecast2/icecast.xml
    sudo sed -i "s/hackme/$secret/g" /etc/icecast2/icecast.xml
    sudo systemctl enable icecast2.service  
    # MAPS
    echo "Configuring MAPS..."
    mkdir -p $HOME/.local/lib
    unzip -q $HOME/suda-git/art/maps/pd.zip -d $HOME/.local/lib
    cp -r $HOME/suda-git/art/maps/ $HOME/
    cp $HOME/suda-git/config/.pdsettings $HOME/.pdsettings
    sed -i "s/user/$USER/g" $HOME/.pdsettings
    sed -i "s/https\:\/\/suda\.hacklab01\.org\:8443/http:\/\/$ip:8080/g" $HOME/suda-git/noVNC-1.3.0/index.html   
    sed -i "s/hackme/$secret/g" $HOME/maps/MAPS.pd
    sed -i "s/changeme/$ip/g" $HOME/maps/MAPS.pd
    echo "Everything is configured for local (ethernet) use."  
    read -r -p "Would you like to setup tunneling (a server with a public IP is requred)? [y/N] " response
    case "$response" in
    [yY][eE][sS]|[yY]) 
        echo "Fetching rathole"
        wget -O $HOME/suda-git/rathole-x86_64-unknown-linux-gnu.zip https://github.com/rapiz1/rathole/releases/download/v0.4.7/rathole-x86_64-unknown-linux-gnu.zip
        echo "Setting up rathole"
        mkdir $HOME/rathole
        unzip -q $HOME/suda-git/rathole-x86_64-unknown-linux-gnu.zip -d $HOME/rathole
        sudo chmod +x $HOME/rathole/rathole
        sudo mv $HOME/rathole/rathole /usr/bin/
        sudo mkdir -p /etc/rathole
        sudo cp $HOME/suda-git/config/app1c.toml /etc/rathole
        sudo cp $HOME/suda-git/config/app2c.toml /etc/rathole
        sudo cp $HOME/suda-git/config/app3c.toml /etc/rathole
        sudo cp $HOME/suda-git/config/ratholec@.service /etc/systemd/system/
        sudo sed -i "s/hackme/$secret/g" /etc/rathole/app1c.toml
        sudo sed -i "s/hackme/$secret/g" /etc/rathole/app2c.toml
        sudo sed -i "s/hackme/$secret/g" /etc/rathole/app3c.toml
        sudo systemctl enable ratholec@app1 --now
        sudo systemctl enable ratholec@app2 --now
        sudo systemctl enable ratholec@app3 --now
        echo "Rathole client is configured!"
        read -p "Enter remote server domain (this script will generate a command you can paste into the terminal there): " domain
        echo "Setting up icecast for tunneling. It will be disabled locally and installed on the server."
        sudo systemctl disable icecast2.serice
        echo "Setting up MAPS for tunneling. It will no longer work without an internet connection and both the server and the client running."
        sed -i "s/$ip/$server/g" $HOME/maps/MAPS.pd
        echo "Login to the server with a public IP address and execute:"
        echo "wget https://gitlab.com/hacklab01/suda/-/raw/main/suda-installer.sh"
        echo "bash suda-installer.sh -d $domain -p $secret"
        read -p "SUDA is installed press Enter to reboot!"
        ;;
    *)
        read -p "SUDA is installed press Enter to reboot!"
        ;;
    esac

    sudo reboot
    exit 0;

elif [[ " ${DISTA[*]} " =~ " ${ID} " ]]; then 
    # Install for Arch based systems
    echo "SUDA installer 🄯 v3d 2022"
    echo "Documentation: https://wiki.formatc.hr/shelves/suda." 
    echo "Installing software..."
    sudo pacman -Sy
    sudo pacman -S git xterm lxterminal vim lxdm x11vnc ctwm python m4 python-pip tk xdotool ffmpeg psmisc pd unzip icecast wget --noconfirm
    echo "Installing Unifont"
    sudo mkdir -p /usr/local/share/fonts/unifont/
    wget https://unifoundry.com/pub/unifont/unifont-15.0.01/font-builds/unifont-15.0.01.ttf
    sudo mv unifont-15.0.01.ttf /usr/local/share/fonts/unifont/ttf-unifont.ttf
    echo "Fetching configs and scripts..."
    git clone https://gitlab.com/hacklab01/suda.git $HOME/suda-git
    echo "Copying ctwm.desktop to /usr/share/xsessions/ so lxdm knows about ctwm..."
    sudo cp $HOME/suda-git/config/ctwm.desktop /usr/share/xsessions/
    echo "Copying .ctwmrc config file to home dir"
    cp $HOME/suda-git/config/.ctwmrc $HOME/
    echo "Copying the lxdm configuration file lxdm.conf to /etc/lxdm overwriting any previous config..."
    sudo cp $HOME/suda-git/config/default.conf /etc/lxdm/lxdm.conf
    echo "Copying scripts to /usr/local/bin and make them executable..."
    sudo cp $HOME/suda-git/scripts/*.sh /usr/local/bin/
    sudo chmod +x /usr/local/bin/*.sh
    echo "Copying PostLogin script for autostart..."
    sudo cp $HOME/suda-git/config/PostLogin /etc/lxdm/
    echo "Attempting to disable current display manager..."
    sudo systemctl disable lightdm
    sudo systemctl disable gdm
    sudo systemctl disable sddm
    sudo systemctl disable xorg-xdm
    echo "Setting lxdm as default display manager..."
    sudo systemctl enable lxdm
    unzip $HOME/suda-git/noVNCv1.3.0.zip -d $HOME/suda-git/
    echo "Fetching noVNC..."
    wget -O $HOME/suda-git/noVNCv1.3.0.zip  https://github.com/novnc/noVNC/archive/refs/tags/v1.3.0.zip
    unzip -q $HOME/suda-git/noVNCv1.3.0.zip -d $HOME/suda-git/
    echo "Setting up SUDA web interface..."
    cp -r $HOME/suda-git/art/web/* $HOME/suda-git/noVNC-1.3.0/
    echo "Copying systemd service files"
    sudo cp $HOME/suda-git/config/*.service /etc/systemd/system/
    sudo systemctl enable novnc.service
    sudo systemctl enable x11vnc.service
    sudo systemctl start novnc.service
    sudo systemctl start x11vnc.service
    # Icecast
    echo "Configuring icecast..."
    sudo cp $HOME/suda-git/config/icecast-local.xml /etc/icecast.xml
    sudo sed -i "s/icecast2/icecast/g" /etc/icecast.xml
    sudo sed -i "s/hackme/$secret/g" /etc/icecast.xml
    sudo systemctl enable icecast.service 
    # MAPS
    echo "Configuring MAPS..."
    mkdir -p $HOME/.local/lib
    unzip -q $HOME/suda-git/art/maps/pd.zip -d $HOME/.local/lib
    cp -r $HOME/suda-git/art/maps/ $HOME/
    cp $HOME/suda-git/config/.pdsettings $HOME/.pdsettings
    sed -i "s/user/$USER/g" $HOME/.pdsettings
    sed -i "s/https\:\/\/suda\.hacklab01\.org\:8443/http:\/\/$ip:8080/g" $HOME/suda-git/noVNC-1.3.0/index.html   
    sed -i "s/hackme/$secret/g" $HOME/maps/MAPS.pd
    sed -i "s/changeme/$ip/g" $HOME/maps/MAPS.pd
    echo "Everything is configured for local (ethernet) use."
    read -p "SUDA is installed press Enter to reboot!"
    sudo reboot
    exit 0;

else 
    #Install for other systems
    read -p "Unable to detertmine system type, check the documentation to install manually! Press enter to exit."
    exit 0; 
fi
else 
    echo "Configuring Rathole server (this should only be run on a server). Ports 8080, 80, and 443 should be avaliable!"
    echo "Installing software..."
    if [[ " ${DISTD[*]} " =~ " ${ID} " ]]; then
        sudo apt install certbot icecast2 unzip
    elif [[ " ${DISTA[*]} " =~ " ${ID} " ]]; then 
        sudo pacman -S certbot icecast unzip
    else 
        #Install for other systems
        read -p "Unable to detertmine system type, check the documentation to install manually! Press enter to exit."
        exit 0; 
    fi
    echo "Seting up SSL. Be sure to enter a valid email address as you will have to refresh this certificate manually."
    sudo certbot certonly -d $domain
    echo "To refresh the certificate issue this command:"
    echo "sudo certbot certonly -d $domain"
    echo "Copying SSL certificate for iceceast..."
    if [[ " ${DISTD[*]} " =~ " ${ID} " ]]; then
        sudo mkdir -p /etc/icecast2
        sudo bash -c "cat /etc/letsencrypt/live/$domain/fullchain.pem /etc/letsencrypt/live/$domain/privkey.pem > /etc/icecast2/bundle.pem"
    elif [[ " ${DISTA[*]} " =~ " ${ID} " ]]; then 
        sudo mkdir -p /etc/icecast
        sudo bash -c "cat /etc/letsencrypt/live/$domain/fullchain.pem /etc/letsencrypt/live/$domain/privkey.pem > /etc/icecast/bundle.pem"
    else     
        #Install for other systems
        read -p "Unable to detertmine system type, check the documentation to install manually! Press enter to exit."
        exit 0; 
    fi
    echo "Fetching Rathole"
    wget -O $HOME/rathole-x86_64-unknown-linux-gnu.zip https://github.com/rapiz1/rathole/releases/download/v0.4.7/rathole-x86_64-unknown-linux-gnu.zip
    echo "Setting up rathole"
    mkdir $HOME/rathole
    unzip -q $HOME/rathole-x86_64-unknown-linux-gnu.zip -d $HOME/rathole
    sudo chmod +x $HOME/rathole/rathole
    sudo mv $HOME/rathole/rathole /usr/bin/
    sudo mkdir -p /etc/rathole
    sudo wget -O /etc/rathole/app1s.toml https://gitlab.com/hacklab01/suda/-/raw/main/config/app1s.toml
    sudo wget -O /etc/rathole/app2s.toml https://gitlab.com/hacklab01/suda/-/raw/main/config/app2s.toml
    sudo wget -O /etc/rathole/app3s.toml https://gitlab.com/hacklab01/suda/-/raw/main/config/app3s.toml
    sudo wget -O /etc/systemd/system/ratholes@.service https://gitlab.com/hacklab01/suda/-/raw/main/config/ratholes@.service
    sudo sed -i "s/hackme/$password/g" /etc/rathole/app1s.toml
    sudo sed -i "s/hackme/$password/g" /etc/rathole/app2s.toml
    sudo sed -i "s/hackme/$password/g" /etc/rathole/app3s.toml
    sudo systemctl enable ratholes@app1 --now
    sudo systemctl enable ratholes@app2 --now
    sudo systemctl enable ratholes@app3 --now
    echo "Allowing rathole to open low ports..."
    sudo setcap 'cap_net_bind_service=+ep' /usr/bin/rathole
    echo "Setting up icecast"
    if [[ " ${DISTD[*]} " =~ " ${ID} " ]]; then
        sudo wget -O /etc/icecast2/icecast.xml https://gitlab.com/hacklab01/suda/-/raw/main/config/icecast-remote.xml 
        sudo sed -i "s/hackme/$password/g" /etc/icecast2/icecast.xml
        sudo sed -i "s/changeme/$domain/g" /etc/icecast2/icecast.xml
        sudo systemctl enable icecast2
    sudo systemctl enable icecast2.service 
    elif [[ " ${DISTA[*]} " =~ " ${ID} " ]]; then 
        sudo wget -O /etc/icecast.xml https://gitlab.com/hacklab01/suda/-/raw/main/config/icecast-remote.xml 
        sudo sed -i "s/hackme/$password/g" /etc/icecast.xml
        sudo sed -i "s/changeme/$domain/g" /etc/icecast.xml
        sudo sed -i "s/icecast2/icecast/g" /etc/icecast.xml
        sudo systemctl enable icecast
    else 
        #Install for other systems
        read -p "Unable to detertmine system type, check the documentation to install manually! Press enter to exit."
        exit 0; 
    fi

fi
